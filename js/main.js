const clearSearchEvent = () => {
    const clearButton = document.querySelector('.search-clear-button');
    const searchInput = document.querySelector('.search-input');

    clearButton.addEventListener('click', () => {
        searchInput.value = '';
        searchInput.focus();
    })
}

const productAmountButtonsEvent = (card) => {
    const increaseButton = card.querySelector('.product-card-amount-button-increase');
    const decreaseButton = card.querySelector('.product-card-amount-button-decrease');
    const amountInput = card.querySelector('.product-card-amount-input');

    increaseButton.addEventListener('click', () => {
        let amount = +amountInput.value || 0;
        ++amount;
        amountInput.value = amount;
    })

    decreaseButton.addEventListener('click', () => {
        let amount = +amountInput.value || 0;
        --amount;
        amountInput.value = amount >= 0 ? amount : 0;
    })

    amountInput.addEventListener('change', () => {
        amountInput.value = +amountInput.value || 0;
    })
}

const setProductCards = async (page = 1, cardElement) => {
    const getProductsPage = () => {
        return fetch(`http://localhost:3000/products?_page=${page}&_limit=4`)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                return data;
            });

    }

    const setCardData = (card, product) => {
        const {
            available,
            code,
            description,
            discount,
            price,
            discount_price,
            new: new_product,
            recommended,
            cover,
            in_cart,
            favorite
        } = product;

        const cardAvailable = card.querySelector('.product-card-available');
        const cardCode = card.querySelector('.product-card-code');
        const cardText = card.querySelector('.product-card-text');
        const cardDiscount = card.querySelector('.product-discount');
        const cardPrice = card.querySelector('.product-card-price');
        const cardOldPrice = card.querySelector('.product-card-old_price');
        const cardNew = card.querySelector('.product-new');
        const cardRecommended = card.querySelector('.product-recommended');
        const cardCover = card.querySelector('.product-card-cover');
        const cardCart = card.querySelector('.product-card-cart');
        const cardFavoriteWrapper = card.querySelector('.product-favorite-wrapper');
        const cardFavorite = card.querySelector('.product-favorite-heart');

        cardCode.innerText = `Код: ${code || 'Неизвестен'}`;
        cardText.innerText = description || 'Нет описания';
        cardDiscount.innerText = description || 'Нет описания';
        cardCover.src = cover;

        if (available) {
            cardAvailable.classList.add('active');
            cardAvailable.innerText = 'В наличии';
        }

        if (new_product) {
            cardNew.classList.remove('d-none');
        }

        if (recommended) {
            cardRecommended.classList.remove('d-none');
        }

        if (in_cart) {
            cardCart.classList.remove('d-none');
            cardCart.querySelector('.cart-counter-wrapper').innerText = in_cart;
        }

        if (favorite) {
            cardFavorite.src = './img/favorite-heart-active.svg';
            cardFavoriteWrapper.classList.add('active');
        }

        if (discount) {
            cardDiscount.classList.remove('d-none');
            cardOldPrice.classList.remove('d-none')
            cardDiscount.innerText = `Акция -${discount}%`;
            cardPrice.innerText = `${discount_price.toFixed(2)} р.`;
            cardOldPrice.innerText = `${price.toFixed(2)} р.`;
        } else {
            cardPrice.innerText = `${price.toFixed(2)} р.`;
        }

        productAmountButtonsEvent(card);

        return card;
    }

    const productsData = await getProductsPage();
    const parentElement = document.querySelector(`.product-carousel-item.item-${page} .row`);

    productsData.forEach((product) => {
        const newProductCard = cardElement.cloneNode(true);
        const updatedProductCard = setCardData(newProductCard, product);

        parentElement.insertBefore(updatedProductCard, newProductCard.previousSibling);
    });
}

const productLoaderEvent = async () => {
    const productCardExample = document.querySelector('.product-card-wrapper');
    const productCard = productCardExample.cloneNode(true);
    const activePage = document.querySelector('#promoBlockCarousel');

    const prevButton = document.querySelector('.promo-controls.carousel-control-prev');
    const nextButton = document.querySelector('.promo-controls.carousel-control-next');

    productCardExample.remove();
    productCard.classList.remove('d-none');

    const getProductsAmount = () => {
        return fetch('http://localhost:3000/products_amount')
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                return data.all;
            });

    }

    const carouselItem = document.querySelector('.product-carousel-item');
    const productsAmount = await getProductsAmount();
    const slidesAmount = Math.floor(productsAmount / 4);
    const slides = Array.from(Array(slidesAmount - 1).keys());

    slides.forEach((_, index) => {
        const newCarouselItem = carouselItem.cloneNode(true);
        newCarouselItem.classList.remove('active');
        newCarouselItem.classList.add(`item-${slides.length - index + 1}`)
        carouselItem.parentNode.insertBefore(newCarouselItem, carouselItem.nextSibling);
    });

    carouselItem.classList.add('item-1');

    setProductCards('1', productCard).then();

    const loadedPages = new Set();
    loadedPages.add('1');

    const setIndicators = (page) => {
        const activeIndicator = document.querySelector('.product-carousel-indicator.active');

        if (!activeIndicator) {
            const allIndicators = document.querySelectorAll('.product-carousel-indicator');

            allIndicators.forEach((indicator, index) => {
                const slideTo = +page + index - 1;
                indicator.dataset.bsSlideTo = `${slideTo}`;

                if (slideTo > slidesAmount) {
                    indicator.classList.add('d-none');
                } else {
                    indicator.classList.remove('d-none');
                }
            })

            const newActiveIndicator = document.querySelector(`.product-carousel-indicator[data-bs-slide-to="${+page - 1}"]`);
            newActiveIndicator.classList.add('active')
        }
    }

    prevButton.addEventListener('click', () => {
        let prevPage = `${+activePage.dataset.activePage - 1}`;

        if (+prevPage < 1) {
            prevPage = `${slidesAmount}`;
        }

        activePage.dataset.activePage = prevPage;
        if (!loadedPages.has(prevPage)) {
            setProductCards(prevPage, productCard).then();
            loadedPages.add(prevPage);
        }

        setIndicators(prevPage);
    });

    nextButton.addEventListener('click', () => {
        let nextPage = `${+activePage.dataset.activePage + 1}`

        if (+nextPage > slidesAmount) {
            nextPage = '1';
        }

        activePage.dataset.activePage = nextPage;

        if (!loadedPages.has(nextPage)) {
            setProductCards(nextPage, productCard).then();
            setIndicators(nextPage);
            loadedPages.add(nextPage);
        }
    });
}

document.addEventListener('DOMContentLoaded', () => {
    clearSearchEvent();
    productLoaderEvent().then();
})